/**
 * 
 */
package it.cybersec.af2.gruppo3.callcenter.parser;

import it.cybersec.af2.gruppo3.callcenter.parser.aggregati.RisultatiParser;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * 
 * Parser for the log file create with Tool1
 * 
 * @author Matteo Mungo
 * @author Clemente Palermo
 * @author Ramona Procopio
 *
 */
public class Parser {
	
	private FileInputStream fis;
	private DataInputStream dis;
	private BufferedReader br;
	
	private File directory;
	private RisultatiParser risultati;
	
	public Parser(String path) {
		directory= new File(path);
		if( !(directory.isDirectory())){
			throw new IllegalArgumentException("Indicare una directory valida al parser");
		}
		risultati= new RisultatiParser();
		
	}
	
	/**
	 * Start the parsing of the file logs
	 */
	public void parse(){
		
		try {
			String baseNomeFile=directory.getAbsolutePath()+"\\call_center.log";
			String nomeFileCompleto;
			File[] listaFile= directory.listFiles();
			File file;
			
			if(listaFile == null){
				throw new IllegalArgumentException();
			}
		
			for(int index=listaFile.length-1 ; index >= 0; index--){
				
				if(index == 0){
					nomeFileCompleto=baseNomeFile;
				}else{
					nomeFileCompleto=baseNomeFile+"."+index;
				}
				file= new File(nomeFileCompleto);
				fis= new FileInputStream(file);
				dis= new DataInputStream(fis);
				br= new BufferedReader(new InputStreamReader(dis));
				String line;
				
				while( (line = br.readLine()) != null){
					String[] result=line.split("\t");
					elaboraStringaFile(result);					
				}
				br.close();
				dis.close();
				fis.close();
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}//end parse
	
	private void elaboraStringaFile(String[] result){
		Operazione operazioneNum= Operazione.valueOf(result[1]);
		int id;
		DatiChiamata tmpDati;
		switch (operazioneNum) {
		case RFTCH :
			risultati.incChiamateRifiutate();
			break;	
		case ARVCD:
			//Segno il tempo di arrivo in coda
			id= Integer.parseInt(result[3]);
			tmpDati=risultati.verificaChiamata(id);
			tmpDati.setTempoIngressoCoda(Long.parseLong(result[2]));
			tmpDati.setTipologia(Tipologia.valueOf(result[4]));
			risultati.aggiornaChiamata(tmpDati);
			break;
		case USTCD:
			//Segno il tempo di uscita dalla coda
			id= Integer.parseInt(result[3]);
			tmpDati=risultati.verificaChiamata(id);
			tmpDati.setTempoUscitaCoda(Long.parseLong(result[2]));
			risultati.aggiornaChiamata(tmpDati);
			break;
		case FNCH:
			//Segno il tempo di fine chiamata
			id= Integer.parseInt(result[3]);
			tmpDati=risultati.verificaChiamata(id);
			tmpDati.setTempoFineChiamata(Long.parseLong(result[2]));
			risultati.aggiornaChiamata(tmpDati);
			//Conteggio la tipologia della chiamata
			Tipologia tipo= Tipologia.valueOf(result[4]);
			risultati.incContatoreTipologia(tipo);
			//Segno per l'operatore result[5] una chiamata
			risultati.incChiamataPerPostazione(Integer.parseInt(result[5]));
			break;
		case INIT:
			risultati.setNumChiamateTotaliPreviste(Integer.parseInt(result[2]));
			risultati.setNumPostazioni((Integer.parseInt(result[3])));
			break;
		default:
			throw new IllegalArgumentException("Operazione non riconosciuta");
		}
	}
	
	/**
	 * Return the reference to the object with the informations to be processed to obtain the statistics
	 * @return RisultatiParser
	 */
	public RisultatiParser getRisultatiParsing(){
		return risultati;
	}
	

}
