package it.cybersec.af2.gruppo3.callcenter.parser.test;

import it.cybersec.af2.gruppo3.callcenter.parser.Parser;
import it.cybersec.af2.gruppo3.callcenter.parser.aggregati.CollezioneStat;
import it.cybersec.af2.gruppo3.callcenter.parser.aggregati.RisultatiParser;
import it.cybersec.af2.gruppo3.callcenter.parser.json.JSONGenerator;

public class Tester {
	public static void main(String[] args) {
		String path=".\\input";
		Parser parser= new Parser(path);
		parser.parse();
		RisultatiParser ris= parser.getRisultatiParsing();
		CollezioneStat collezione= new CollezioneStat();
		collezione.riempiCollezione(ris);
		JSONGenerator.generaJSON(collezione);
	}
	
}
