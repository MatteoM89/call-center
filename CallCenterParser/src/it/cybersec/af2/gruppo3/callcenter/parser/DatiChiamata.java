package it.cybersec.af2.gruppo3.callcenter.parser;

/**
 * A single object that contains informations about a generic call.
 * 
 * @author Matteo Mungo
 * @author Clemente Palermo
 * @author Ramona Procopio
 *
 */
public class DatiChiamata {
	
	private Integer id;
	private Long tempoIngressoCoda;
	//Coincide con la fine della chiamata
	private Long tempoUscitaCoda;
	private Long tempoFineChiamata;
	private Tipologia tipo;
	
	public DatiChiamata(Integer id) {
		this.id=id;
	}
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public void setTempoIngressoCoda(Long tempoIngressoCoda) {
		this.tempoIngressoCoda = tempoIngressoCoda;
	}
	
	public void setTempoUscitaCoda(Long tempoUscitaCoda) {
		this.tempoUscitaCoda = tempoUscitaCoda;
	}
	
	public void setTempoFineChiamata(Long fineChiamata) {
		this.tempoFineChiamata = fineChiamata;
	}
	
	/**
	 * Return the lifespan of a call 
	 * @return Long the lifespan
	 */
	public Long getDurataChiamata(){
		return (this.tempoFineChiamata - this.tempoUscitaCoda);
	}
	
	/**
	 * Return the lifespan of the waiting in the queue
	 * @return Long the lifespan of waiting
	 */
	public Long getDurataAttesaInCoda() {
		return (this.tempoUscitaCoda-this.tempoIngressoCoda);
	}
	
	/**
	 * Return the total lifespan of the service
	 * @return Long the lifespan of the service
	 */
	public Long getTempoDiRisposta(){
		return (this.tempoFineChiamata-this.tempoIngressoCoda);
	}
	
	public void setTipologia(Tipologia tipo){
		this.tipo=tipo;
	}
	
	public Tipologia getTipologia(){
		return this.tipo;
	}

}
