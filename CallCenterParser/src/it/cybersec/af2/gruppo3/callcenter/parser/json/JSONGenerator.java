package it.cybersec.af2.gruppo3.callcenter.parser.json;

import it.cybersec.af2.gruppo3.callcenter.parser.aggregati.CollezioneStat;

import java.io.FileWriter;
import java.io.IOException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class JSONGenerator {
	
	public static void generaJSON(CollezioneStat collezione){
		Gson builder= new GsonBuilder().create();
		String res=builder.toJson(collezione);
		try {  
			   //write converted json data to a file named "CountryGSON.json"  
			   FileWriter writer = new FileWriter(".\\output\\output.json");  
			   writer.write(res);  
			   writer.close();  
			    
		} catch (IOException e) {  
			e.printStackTrace();  
		}  

	}

}
