package it.cybersec.af2.gruppo3.callcenter.parser.aggregati;

import it.cybersec.af2.gruppo3.callcenter.parser.DatiChiamata;
import it.cybersec.af2.gruppo3.callcenter.parser.Tipologia;

import java.util.HashMap;
import java.util.Map;

/**
 * Object to store and preprocess the statistics
 * @author Matteo Mungo
 * @author Clemente Palermo
 * @author Ramona Procopio
 *
 */
public class RisultatiParser {
	
	private Map<Integer, DatiChiamata> logChiamate;
	private Integer chiamateRifiutate;
	private Integer[] chiamatePerPostazione;
	private int numChiamateTotaliPreviste;
	private int[] conteggioTipologia; //0 --> guasto 1 --> informazione
	
	/**
	 * Default constructor
	 */
	public RisultatiParser() {
		logChiamate= new HashMap<Integer, DatiChiamata>();
		chiamateRifiutate=0;
		numChiamateTotaliPreviste=0;
		conteggioTipologia= new int[2];
	}
	
	/**
	 * Save the number of operators in the Call Center
	 * @param the number of operators
	 */
	public void setNumPostazioni(int id){
		chiamatePerPostazione= new Integer[id];
		for(int i=0; i<chiamatePerPostazione.length; i++){
			chiamatePerPostazione[i]=0;
		}
	}
	
	/**
	 * Return the number of operators in the call center
	 * @return the number of operators
	 */
	public int getNumeroPostazioni() {
		return chiamatePerPostazione.length;
	}
	
	/**
	 * Initialize the collection of DatiChiamata for the calls
	 * @param Map<Integer, DatiChiamata> with the calls 
	 */
	public void setLogChiamate(Map<Integer, DatiChiamata> logChiamate) {
		this.logChiamate = logChiamate;
	}
	
	/**
	 * Check if the call is already saved in the collection
	 * @param id of the call
	 * @return the previous instance of the call or a new instance
	 */
	public DatiChiamata verificaChiamata(Integer id){
		DatiChiamata tmpChiamata;
		if(logChiamate.containsKey(id)){
			tmpChiamata= logChiamate.remove(id);
		}else{
			tmpChiamata= new DatiChiamata(id);
		}
		return tmpChiamata;
	}
	/**
	 * Increment of one unit the count for type of call
	 * @param the type of call
	 */
	public void incContatoreTipologia(Tipologia tipo){
		if(tipo.equals(Tipologia.AssistenzaGuasto)){
			conteggioTipologia[0]++;
		}else{
			conteggioTipologia[1]++;
		}
	}
	
	/**
	 * Increment of one unit the number of calls for the specific operator
	 * @param the id of the operator
	 */
	public void incChiamataPerPostazione(Integer idPostazione){
		chiamatePerPostazione[idPostazione]++;
	}
	
	/**
	 * Update the informations of the call in the collection
	 * @param the instance of the call
	 */
	public void aggiornaChiamata(DatiChiamata chiamata){
		logChiamate.put(chiamata.getId(), chiamata);
	}
	
	/**
	 * Increse of one unit the number of rejected calls
	 */
	public void incChiamateRifiutate() {
		this.chiamateRifiutate ++;
	}
	
	/**
	 * Set the number of expected calls
	 * @param the number of expected calls
	 */
	public void setNumChiamateTotaliPreviste(int numChiamateTotaliPreviste) {
		this.numChiamateTotaliPreviste = numChiamateTotaliPreviste;
	}
	
	/**
	 * Return the number of rejected calls
	 * @return the number of rejected calls
	 */
	public int getNumChiamateRifiutate(){
		return chiamateRifiutate;
	}
	
	/**
	 * Return the percentage of call of assistance
	 * @return the percentage
	 */
	public double getPercentualeChiamateGuasti(){
		double ris=(((double)conteggioTipologia[0])*100)/((double)numChiamateTotaliPreviste);
		
		return ris;
	}
	
	/**
	 * Return the percentage of call of informations
	 * @return the percentage
	 */
	public double getPercentualeChiamateInformazione(){
		double ris=(((double)conteggioTipologia[1])*100)/((double)numChiamateTotaliPreviste);
		return ris;
	}
	
	/**
	 * Return the mean time of waiting in the queue for a call
	 * @return the mean time
	 */
	public double getTempoMedioAttesaCoda(){
		double tempoMedio=0.0;
		
		for(Integer id: logChiamate.keySet()){
			DatiChiamata chiamata= logChiamate.get(id);
			tempoMedio += ((double)chiamata.getDurataAttesaInCoda());
		}
		return ((tempoMedio/numChiamateTotaliPreviste)/1000);
	}
	
	/**
	 * Return the mean time of waiting in the queue for the calls of assistance
	 * @return the mean time
	 */
	public double getTempoMedioAttesaCodaGuasti(){
		double tempoMedio=0.0;
		for(Integer id: logChiamate.keySet()){
			DatiChiamata chiamata= logChiamate.get(id);
			if(chiamata.getTipologia().equals(Tipologia.AssistenzaGuasto)){
				tempoMedio += ((double)chiamata.getDurataAttesaInCoda());
			}
		}
		return ((tempoMedio/conteggioTipologia[0])/1000);
	}
	
	/**
	 * Return the mean time of waiting in the queue for the calls of information
	 * @return the mean time
	 */
	public double getTempoMedioAttesaCodaInformazioni(){
		double tempoMedio=0.0;
		for(Integer id: logChiamate.keySet()){
			DatiChiamata chiamata= logChiamate.get(id);
			if(chiamata.getTipologia().equals(Tipologia.Informazione)){
				tempoMedio += ((double)chiamata.getDurataAttesaInCoda());
			}
		}
		return ((tempoMedio/conteggioTipologia[1])/1000);
	}
	
	/**
	 * Return the max time of waiting in the queue
	 * @return the max mean time 
	 */
	public double getTempoAttesaCodaMax(){
		double tempoMedio=logChiamate.get(0).getDurataAttesaInCoda();
		
		for(Integer id: logChiamate.keySet()){
			DatiChiamata chiamata= logChiamate.get(id);
			if(tempoMedio < chiamata.getDurataAttesaInCoda()){
				tempoMedio=((double)chiamata.getDurataAttesaInCoda());
			}
		}
		return (tempoMedio/1000);
	}
	
	/**
	 * Return the min time of waiting in the queue
	 * @return the min mean time
	 */
	public double getTempoAttesaCodaMin(){
		double tempoMedio=logChiamate.get(0).getDurataAttesaInCoda();
		
		for(Integer id: logChiamate.keySet()){
			DatiChiamata chiamata= logChiamate.get(id);
			if(tempoMedio > chiamata.getDurataAttesaInCoda()){
				tempoMedio=((double)chiamata.getDurataAttesaInCoda());
			}
		}
		return (tempoMedio/1000);
	}
	
	/**
	 * Return the mean time of duration of a call
	 * @return the mean time 
	 */
	public double getTempoMedioDurataChiamata(){
		double tempoMedio=0.0;
		
		for(Integer id: logChiamate.keySet()){
			DatiChiamata chiamata= logChiamate.get(id);
			tempoMedio += ((double)chiamata.getDurataChiamata());
		}
		return ((tempoMedio/numChiamateTotaliPreviste)/1000);
	}
	
	/**
	 * Return the mean time of duration of a call of assistence
	 * @return the mean time
	 */
	public double getTempoMedioDurataChiamataGuasti(){
		double tempoMedio=0.0;
		
		for(Integer id: logChiamate.keySet()){
			DatiChiamata chiamata= logChiamate.get(id);
			if(chiamata.getTipologia().equals(Tipologia.AssistenzaGuasto)){
				tempoMedio += ((double)chiamata.getDurataChiamata());
			}
		}
		return ((tempoMedio/conteggioTipologia[0])/1000);
	}
	
	/**
	 * Return the mean time of duration of a call of information
	 * @return the mean time
	 */
	public double getTempoMedioDurataChiamataInformazioni(){
		double tempoMedio=0.0;
		
		for(Integer id: logChiamate.keySet()){
			DatiChiamata chiamata= logChiamate.get(id);
			if(chiamata.getTipologia().equals(Tipologia.Informazione)){
				tempoMedio += ((double)chiamata.getDurataChiamata());
			}
		}
		return ((tempoMedio/conteggioTipologia[1])/1000);
	}
	
	/**
	 * Return the total number of calls
	 * @return the total number of calls
	 */
	public int getChiamateTotali(){
		int ris=0;
		for(int i=0; i<chiamatePerPostazione.length; i++){
			ris+=chiamatePerPostazione[i];
		}
		return ris;
	}
	
	/**
	 * Return the total number of calls for a single operator
	 * @param id of the operator
	 * @return the total calls for the operator
	 */
	public int getChiamateTotaliPerPostazione(int id){
		return chiamatePerPostazione[id];
	}
	
	/**
	 * Return the productivity of a operator
	 * @param id of the operator
	 * @return the productivity
	 */
	public double getProduttivitaPostazione(int id){
		return (((double)chiamatePerPostazione[id])*100)/((double)numChiamateTotaliPreviste);
	}
	
	/**
	 * Return the the total number of calls expected for the simulation
	 * @return the total number of calls expected for the simulation
	 */
	public int getChiamateTotaliPreviste(){
		return numChiamateTotaliPreviste;
	}
	
	/**
	 * Return the max time of duration of a call
	 * @return the max mean time
	 */
	public double getTempoDurataChiamataMax(){
		double tempoMedio=((double)logChiamate.get(0).getDurataChiamata());
		
		for(Integer id: logChiamate.keySet()){
			DatiChiamata chiamata= logChiamate.get(id);
			if(tempoMedio < chiamata.getDurataChiamata()){
				tempoMedio=((double)chiamata.getDurataChiamata());
			}
		}
		return (tempoMedio/1000);
	}
	
	/**
	 * Return the min time of duration of a call
	 * @return the min mean time
	 */
	public double getTempoDurataChiamataMin(){
		double tempoMedio=((double)logChiamate.get(0).getDurataChiamata());
		
		for(Integer id: logChiamate.keySet()){
			DatiChiamata chiamata= logChiamate.get(id);
			if(tempoMedio > chiamata.getDurataChiamata()){
				tempoMedio=((double)chiamata.getDurataChiamata());
			}
		}
		return (tempoMedio/1000);
		
	}
	
	/**
	 * Return the mean time of service
	 * @return the mean time
	 */
	public double getTempoMedioServizio(){
		double tempoMedio=0.0;
		
		for(Integer id:logChiamate.keySet()){
			DatiChiamata chiamata=logChiamate.get(id);
			tempoMedio+=((double)chiamata.getTempoDiRisposta());
		}
		
		return ((tempoMedio/((double)numChiamateTotaliPreviste))/1000);
	}
	
	/**
	 * Return the mean time of service for an information call
	 * @return the mean time
	 */
	public double getTempoMedioServizioGuasti(){
		double tempoMedio=0.0;
		
		for(Integer id:logChiamate.keySet()){
			DatiChiamata chiamata=logChiamate.get(id);
			if(chiamata.getTipologia().equals(Tipologia.AssistenzaGuasto)){
				tempoMedio+=((double)chiamata.getTempoDiRisposta());
			}
		}
		
		return ((tempoMedio/((double)conteggioTipologia[0]))/1000);
	}
	
	/**
	 * Return the mean time of service for an assistence call
	 * @return the mean time
	 */
	public double getTempoMedioServizioInformazioni(){
		double tempoMedio=0.0;
		
		for(Integer id:logChiamate.keySet()){
			DatiChiamata chiamata=logChiamate.get(id);
			if(chiamata.getTipologia().equals(Tipologia.Informazione)){
				tempoMedio+=((double)chiamata.getTempoDiRisposta());
			}
		}
		
		return ((tempoMedio/((double)conteggioTipologia[1]))/1000);
		
	}
	
	/**
	 * Return the max time of service
	 * @return the max mean time
	 */
	public double getTempoServizioMax(){
		double tempoMedio=((double)logChiamate.get(0).getTempoDiRisposta());
		
		for(Integer id:logChiamate.keySet()){
			DatiChiamata chiamata=logChiamate.get(id);
			if(tempoMedio < chiamata.getTempoDiRisposta()){ 
				tempoMedio = ((double)chiamata.getTempoDiRisposta());
			}
		}
		
		return (tempoMedio/1000);
	}
	
	/**
	 * Return the min time of service
	 * @return the min mean time
	 */
	public double getTempoServizioMin() {
		double tempoMedio=((double)logChiamate.get(0).getTempoDiRisposta());
		
		for(Integer id:logChiamate.keySet()){
			DatiChiamata chiamata=logChiamate.get(id);
			if(tempoMedio > chiamata.getTempoDiRisposta()){ 
				tempoMedio = ((double)chiamata.getTempoDiRisposta());
			}
		}
		
		return (tempoMedio/1000);
	}
	
}
