package it.cybersec.af2.gruppo3.callcenter.parser.aggregati;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class CollezioneStat implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8579124467316300632L;
	private int numChiamateTotali;
	private int numChiamateServite;
	private int numPostazioni;
	private int numChiamateRifiutate;
	private double percentualeChiamateGuasti;
	private double percentualeChiamateInformazioni;
	private double tempoMedioAttesaInCoda;
	private double tempoMedioAttesaInCodaAssistenza;
	private double tempoMedioAttesaInCodaInformazione;
	private double tempoAttesaInCodaMax;
	private double tempoAttesaInCodaMin;
	private double tempoMedioDurataChiamata;
	private double tempoMedioDurataChiamataAssistenza;
	private double tempoMedioDurataChiamataInformazione;
	private double tempoDurataChiamataMax;
	private double tempoDurataChiamataMin;
	private Map<Integer, Integer> numChiamataPerPostazione;
	private Map<Integer, Double> produttivitaPerPostazione;
	private double tempoMedioServizio;
	private double tempoMedioServizioAssistenza;
	private double tempoMedioServizioInformazione;
	private double tempoServizioMax;
	private double tempoServizioMin;
	
	public int getNumChiamateTotali() {
		return numChiamateTotali;
	}
	public void setNumChiamateTotali(int numChiamateTotali) {
		this.numChiamateTotali = numChiamateTotali;
	}
	public int getNumChiamateServite() {
		return numChiamateServite;
	}
	public void setNumChiamateServite(int numChiamateServite) {
		this.numChiamateServite = numChiamateServite;
	}
	public int getNumPostazioni() {
		return numPostazioni;
	}
	public void setNumPostazioni(int numPostazioni) {
		this.numPostazioni = numPostazioni;
	}
	public int getNumChiamateRifiutate() {
		return numChiamateRifiutate;
	}
	public void setNumChiamateRifiutate(int numChiamateRifiutate) {
		this.numChiamateRifiutate = numChiamateRifiutate;
	}
	public double getPercentualeChiamateGuasti() {
		return percentualeChiamateGuasti;
	}
	public void setPercentualeChiamateGuasti(double percentualeChiamateGuasti) {
		this.percentualeChiamateGuasti = percentualeChiamateGuasti;
	}
	public double getPercentualeChiamateInformazioni() {
		return percentualeChiamateInformazioni;
	}
	public void setPercentualeChiamateInformazioni(
			double percentualeChiamateInformazioni) {
		this.percentualeChiamateInformazioni = percentualeChiamateInformazioni;
	}
	public double getTempoMedioAttesaInCoda() {
		return tempoMedioAttesaInCoda;
	}
	public void setTempoMedioAttesaInCoda(double tempoMedioAttesaInCoda) {
		this.tempoMedioAttesaInCoda = tempoMedioAttesaInCoda;
	}
	public double getTempoMedioAttesaInCodaAssistenza() {
		return tempoMedioAttesaInCodaAssistenza;
	}
	public void setTempoMedioAttesaInCodaAssistenza(
			double tempoMedioAttesaInCodaAssistenza) {
		this.tempoMedioAttesaInCodaAssistenza = tempoMedioAttesaInCodaAssistenza;
	}
	public double getTempoMedioAttesaInCodaInformazione() {
		return tempoMedioAttesaInCodaInformazione;
	}
	public void setTempoMedioAttesaInCodaInformazione(
			double tempoMedioAttesaInCodaInformazione) {
		this.tempoMedioAttesaInCodaInformazione = tempoMedioAttesaInCodaInformazione;
	}
	public double getTempoAttesaInCodaMax() {
		return tempoAttesaInCodaMax;
	}
	public void setTempoAttesaInCodaMax(double tempoAttesaInCodaMax) {
		this.tempoAttesaInCodaMax = tempoAttesaInCodaMax;
	}
	public double getTempoAttesaInCodaMin() {
		return tempoAttesaInCodaMin;
	}
	public void setTempoAttesaInCodaMin(double tempoAttesaInCodaMin) {
		this.tempoAttesaInCodaMin = tempoAttesaInCodaMin;
	}
	public double getTempoMedioDurataChiamata() {
		return tempoMedioDurataChiamata;
	}
	public void setTempoMedioDurataChiamata(double tempoMedioDurataChiamata) {
		this.tempoMedioDurataChiamata = tempoMedioDurataChiamata;
	}
	public double getTempoMedioDurataChiamataAssistenza() {
		return tempoMedioDurataChiamataAssistenza;
	}
	public void setTempoMedioDurataChiamataAssistenza(
			double tempoMedioDurataChiamataAssistenza) {
		this.tempoMedioDurataChiamataAssistenza = tempoMedioDurataChiamataAssistenza;
	}
	public double getTempoMedioDurataChiamataInformazione() {
		return tempoMedioDurataChiamataInformazione;
	}
	public void setTempoMedioDurataChiamataInformazione(
			double tempoMedioDurataChiamataInformazione) {
		this.tempoMedioDurataChiamataInformazione = tempoMedioDurataChiamataInformazione;
	}
	public double getTempoDurataChiamataMax() {
		return tempoDurataChiamataMax;
	}
	public void setTempoDurataChiamataMax(double tempoDurataChiamataMax) {
		this.tempoDurataChiamataMax = tempoDurataChiamataMax;
	}
	public double getTempoDurataChiamataMin() {
		return tempoDurataChiamataMin;
	}
	public void setTempoDurataChiamataMin(double tempoDurataChiamataMin) {
		this.tempoDurataChiamataMin = tempoDurataChiamataMin;
	}
	public Map<Integer, Integer> getNumChiamataPerPostazione() {
		return numChiamataPerPostazione;
	}
	public void setNumChiamataPerPostazione(Map<Integer, Integer> numChiamataPerPostazione) {
		this.numChiamataPerPostazione = numChiamataPerPostazione;
	}
	public Map<Integer, Double> getProduttivitaPerPostazione() {
		return produttivitaPerPostazione;
	}
	public void setProduttivitaPerPostazione(Map<Integer, Double> produttivitaPerPostazione) {
		this.produttivitaPerPostazione = produttivitaPerPostazione;
	}
	public double getTempoMedioServizio() {
		return tempoMedioServizio;
	}
	public void setTempoMedioServizio(double tempoMedioServizio) {
		this.tempoMedioServizio = tempoMedioServizio;
	}
	public double getTempoMedioServizioAssistenza() {
		return tempoMedioServizioAssistenza;
	}
	public void setTempoMedioServizioAssistenza(double tempoMedioServizioAssistenza) {
		this.tempoMedioServizioAssistenza = tempoMedioServizioAssistenza;
	}
	public double getTempoMedioServizioInformazione() {
		return tempoMedioServizioInformazione;
	}
	public void setTempoMedioServizioInformazione(
			double tempoMedioServizioInformazione) {
		this.tempoMedioServizioInformazione = tempoMedioServizioInformazione;
	}
	public double getTempoServizioMax() {
		return tempoServizioMax;
	}
	public void setTempoServizioMax(double tempoServizioMax) {
		this.tempoServizioMax = tempoServizioMax;
	}
	public double getTempoServizioMin() {
		return tempoServizioMin;
	}
	public void setTempoServizioMin(double tempoServizioMin) {
		this.tempoServizioMin = tempoServizioMin;
	}
	
	public void riempiCollezione(RisultatiParser parser){
		this.numChiamateTotali=parser.getChiamateTotaliPreviste();
		this.numChiamateServite=parser.getChiamateTotali();
		this.numPostazioni=parser.getNumeroPostazioni();
		this.numChiamateRifiutate=parser.getNumChiamateRifiutate();
		this.percentualeChiamateGuasti=parser.getPercentualeChiamateGuasti();
		this.percentualeChiamateInformazioni=parser.getPercentualeChiamateInformazione();
		this.tempoMedioAttesaInCoda=parser.getTempoMedioAttesaCoda();
		this.tempoMedioAttesaInCodaAssistenza=parser.getTempoMedioAttesaCodaGuasti();
		this.tempoMedioAttesaInCodaInformazione=parser.getTempoMedioAttesaCodaInformazioni();
		this.tempoAttesaInCodaMax=parser.getTempoAttesaCodaMax();
		this.tempoAttesaInCodaMin=parser.getTempoAttesaCodaMin();
		this.tempoMedioDurataChiamata=parser.getTempoMedioDurataChiamata();
		this.tempoMedioDurataChiamataAssistenza=parser.getTempoMedioDurataChiamataGuasti();
		this.tempoMedioDurataChiamataInformazione=parser.getTempoMedioDurataChiamataInformazioni();
		this.tempoDurataChiamataMax=parser.getTempoDurataChiamataMax();
		this.tempoDurataChiamataMin=parser.getTempoDurataChiamataMin();
		this.tempoMedioServizio=parser.getTempoMedioServizio();
		this.tempoMedioServizioAssistenza=parser.getTempoMedioServizioGuasti();
		this.tempoMedioServizioInformazione=parser.getTempoMedioServizioInformazioni();
		this.tempoServizioMax=parser.getTempoServizioMax();
		this.tempoServizioMin=parser.getTempoServizioMin();
		
		numChiamataPerPostazione= new HashMap<Integer, Integer>();
		produttivitaPerPostazione= new HashMap<Integer, Double>();
		
		for(int i=0; i<numPostazioni; i++){
			this.numChiamataPerPostazione.put(i, parser.getChiamateTotaliPerPostazione(i));
			this.produttivitaPerPostazione.put(i, parser.getProduttivitaPostazione(i));
		}

	}

}
