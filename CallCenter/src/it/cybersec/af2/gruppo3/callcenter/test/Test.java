package it.cybersec.af2.gruppo3.callcenter.test;

import it.cybersec.af2.gruppo3.callcenter.infrastruttura.CallCenter;
import it.cybersec.af2.gruppo3.callcenter.infrastruttura.Operazione;
import it.cybersec.af2.gruppo3.callcenter.utente.Chiamata;

import org.apache.log4j.Logger;

public class Test {
	
	private static Logger logger=Logger.getLogger(Test.class);
	
	public static void main(String[] args) {
		int numOperatore=50;
		int limiteChiamatePausa=250;
		int limiteCode=650;
		CallCenter cc= new CallCenter(numOperatore, limiteChiamatePausa, limiteCode);
		int numChiamateTotali=10000;
		logger.info("\t"+Operazione.INIT.name()+"\t"+numChiamateTotali+"\t"+numOperatore+"\t"+limiteChiamatePausa+"\t"+limiteCode);
		for(int i=0; i<numChiamateTotali; i++){
			Chiamata c= new Chiamata(i, cc);
			new Thread(c).start();
		}
	}

}
