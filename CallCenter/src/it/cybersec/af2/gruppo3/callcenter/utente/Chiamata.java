package it.cybersec.af2.gruppo3.callcenter.utente;

import it.cybersec.af2.gruppo3.callcenter.infrastruttura.CallCenter;

import java.util.Random;

import org.apache.log4j.Logger;

/**
 * Simulates the execution of a call to the call center
 * 
 * @author Matteo Mungo
 * @author Clemente Palermo
 * @author Ramona Procopio
 *
 */
public class Chiamata implements Runnable {
	
	private Integer id;
	private Tipologia tipo;
	private CallCenter callCenter;
	
	private Random random= new Random();
	
	private Logger logger= Logger.getLogger(Chiamata.class);
	
	private static double PERCENTUALE_CHIAMATE=0.35;
	private static int TEMPO_DURATA_CHIAMATA_MIN=700;
	private static int TEMPO_DURATA_CHIAMATA_MAX=1500;
	private static int TEMPO_CHIAMATA_MIN=200;
	private static int TEMPO_CHIAMATA_MAX=700;
	private static int TEMPO_RICHIAMATA_MIN=500;
	private static int TEMPO_RICHIAMATA_MAX=1200;
	
	public Chiamata(Integer id, CallCenter cc) {
		this.id = id;
		this.callCenter=cc;
		this.tipo=(random.nextDouble()<=PERCENTUALE_CHIAMATE)?Tipologia.Informazione:Tipologia.AssistenzaGuasto;
	}

	@Override
	public void run() {
	
		try {
			Thread.sleep((random.nextInt(TEMPO_CHIAMATA_MAX-TEMPO_CHIAMATA_MIN)+TEMPO_CHIAMATA_MIN));
			while(! (callCenter.chiamataInoltrata(id, tipo)) ){
				Thread.sleep((random.nextInt(TEMPO_RICHIAMATA_MAX-TEMPO_RICHIAMATA_MIN)+TEMPO_RICHIAMATA_MIN));
			}
			callCenter.chiamataAccettata(id, tipo);
			Thread.sleep( (random.nextInt(TEMPO_DURATA_CHIAMATA_MAX-TEMPO_DURATA_CHIAMATA_MIN)+TEMPO_DURATA_CHIAMATA_MIN));
			callCenter.chiamataTerminata(id, tipo);
		} catch (Exception e) {
			logger.error("run() - id= "+id+" - Error "+e);
		}
			
	}

}
