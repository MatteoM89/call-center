/**
 * 
 */
package it.cybersec.af2.gruppo3.callcenter.infrastruttura;

import it.cybersec.af2.gruppo3.callcenter.utente.Tipologia;

import java.util.LinkedList;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.log4j.Logger;

/**
 * 
 * Object that manages a inbound Call Center life cycle
 * 
 * @author Matteo Mungo
 * @author Clemente Palermo
 * @author Ramona Procopio
 *
 */
public class CallCenter {
	
	private static Logger logger= Logger.getLogger(CallCenter.class);
	
	private LinkedList<Integer> attesaAssistenzaGuasti;
	private LinkedList<Integer> attesaInformazioni;
	private Postazione[] postazioni;
	
	private int limiteChiamatePausa;
	private static int LIMITE_CODE;
	private int numPostazioniLibere;
	private int numOperatoriAssistenza;
	private int numOperatoriInformazioni;
	private int postazioneInPausa;
	private int chiamateTotaliServite=0;
	
	private Lock lock;
	private Condition operatoriOccupati;
	
	public CallCenter(int numPostazioni, int limiteChiamatePausa, int limiteCode) {
		this.postazioni= new Postazione[numPostazioni];
		this.limiteChiamatePausa=limiteChiamatePausa;
		inizializzaPostazioni();
		numPostazioniLibere=numPostazioni;
		
		numOperatoriAssistenza=0;
		numOperatoriInformazioni=0;
		postazioneInPausa=-1;
		
		LIMITE_CODE=limiteCode;
		attesaAssistenzaGuasti= new LinkedList<Integer>();
		attesaInformazioni= new LinkedList<Integer>();
		
		lock= new ReentrantLock();
		new ReentrantLock();
		operatoriOccupati=lock.newCondition();
		
		new ReentrantLock();
	}
	
	private void inizializzaPostazioni() {
		for(int i=0; i<postazioni.length; i++){
			postazioni[i]= new Postazione(i,limiteChiamatePausa );
		}
	}

	/**
	 * A call try to be scheduled to be served by an operator
	 * @param id of the call
	 * @param type of call
	 * @return true if the call is scheduled, false if it doesn't
	 */
	public boolean chiamataInoltrata(Integer id, Tipologia tipo) {
		lock.lock();
		logger.debug("chiamataInoltrata("+id+", "+tipo.name()+") - Inizio");
		try {
			
			if(tipo.name().equals( Tipologia.AssistenzaGuasto.name() ) ){
				if(attesaAssistenzaGuasti.size()<LIMITE_CODE){
					logger.info("\t"+Operazione.ARVCD.name()+"\t"+System.currentTimeMillis()+"\t"+id+"\t"+tipo.name());
					attesaAssistenzaGuasti.addLast(id);
				}else{
					while(attesaAssistenzaGuasti.size()==LIMITE_CODE){
						logger.info("\t"+Operazione.RFTCH.name());
						return false;						
					}
					attesaAssistenzaGuasti.addLast(id);
				}
			} else {
				if(attesaInformazioni.size()<LIMITE_CODE){
					logger.info("\t"+Operazione.ARVCD.name()+"\t"+System.currentTimeMillis()+"\t"+id+"\t"+tipo.name());
					attesaInformazioni.addLast(id);
				}else{
					while(attesaInformazioni.size()==LIMITE_CODE){
						logger.info("\t"+Operazione.RFTCH.name());
//						listeOccupate.await();
						return false;
					}
					attesaInformazioni.addLast(id);
				}
			}
			
		} catch (Exception e) {
			// logger
			logger.error("chiamataInoltrata("+id+", "+tipo.name()+") Errore: "+e);
		} finally {
			lock.unlock();
		}
		logger.debug("chiamataInoltrata("+id+", "+tipo.name()+") - Fine");
		return true;
	}
	
	public void chiamataAccettata(Integer id, Tipologia tipo) {
		lock.lock();
		logger.debug("chiamataAccettata("+id+", "+tipo.name()+") - Inizio");
		try {
			while(!operatoreDisponibile(id)){
				logger.debug("chiamataAccettata("+id+", "+tipo.name()+") - operatore non disponibile, attendo");
				operatoriOccupati.await();
			}
			occupaPostazioneDisponibile(id, tipo);
			
			if(tipo.name().equals( Tipologia.AssistenzaGuasto.name() ) ){
				attesaAssistenzaGuasti.remove(id);
			} else {
				attesaInformazioni.remove(id);
			}
			
			logger.debug("chiamataAccettata("+id+", "+tipo.name()+") - Fine (chiamata iniziata)");
		} catch (Exception e) {
			logger.error("chiamataAccettata("+id+", "+tipo.name()+" Errore: "+e);
		}finally{
			lock.unlock();
		}
		
	}
	
	private void occupaPostazioneDisponibile(Integer idChiamata, Tipologia tipo){
		logger.trace("occupaPostazioneDisponibile("+idChiamata+", "+tipo.name()+") - Inizio");
		for(int i=0; i<postazioni.length; i++){
			if(postazioni[i].isDisponibile()){
				postazioni[i].setDisponibile(false);
				postazioni[i].setIdChiamataServita(idChiamata);
				postazioni[i].aggiungiChiamataRicevuta();
				numPostazioniLibere--;
				logger.info("\t"+Operazione.USTCD.name()+"\t"+System.currentTimeMillis()+"\t"+idChiamata+"\t"+tipo.name()+"\t"+i);
				break;
			}
		}
			
		if( tipo.name().equals ( (Tipologia.AssistenzaGuasto.name()) ) ) {
			numOperatoriAssistenza++;
		}else{
			numOperatoriInformazioni++;
		}
		logger.trace("occupaPostazioneDisponibile("+idChiamata+", "+tipo.name()+") - Fine");
	}
	
	private boolean operatoreDisponibile(Integer idChiamata) {
		
		logger.trace("operatoreDisponibile("+idChiamata+") - Fine");
		if(numPostazioniLibere==0){
			return false;
		}
		
		//controllo se una delle due code � vuota cos� da scegliere direttamente quella popolata
		if(attesaAssistenzaGuasti.size()==0 && attesaInformazioni.size()>0){
			return attesaInformazioni.getFirst().intValue()==idChiamata;
		}else if(attesaInformazioni.size()==0 && attesaAssistenzaGuasti.size()>0){
			return attesaAssistenzaGuasti.getFirst().intValue()==idChiamata;
		}else if(attesaAssistenzaGuasti.size()>0 && attesaInformazioni.size()>0 ){
			//Se entrambe sono popolate seleziono in maniera dinamica da quale lista scegliere la prossima chiamata da elaborare
			if(sceltaChiamataGuasto())
				return attesaAssistenzaGuasti.getFirst().intValue() == idChiamata;
		}
		logger.trace("operatoreDisponibile("+idChiamata+") - Fine");
		return attesaInformazioni.getFirst().intValue()==idChiamata;
	}
	
	//Permette di scegliere in maniera dinamica quale tipologia di chiamata servire
	private boolean sceltaChiamataGuasto() {
		if(numOperatoriAssistenza==0){
			return true;
		}else if(numOperatoriInformazioni==0){
			return false;
		}
			
		double rapportoOperatori=((double)numOperatoriInformazioni)/((double)numOperatoriAssistenza);
		double rapportoCode= ((double)attesaInformazioni.size())/((double)(attesaAssistenzaGuasti.size()*2));
//		double rapportoGuasti=(numOperatoriAssistenza+0.1)/attesaAssistenzaGuasti.size();
//		double rapportoInfo=(numOperatoriInformazioni+0.1)/attesaInformazioni.size();
		return rapportoOperatori>rapportoCode;
	}

	public void chiamataTerminata(Integer id, Tipologia tipo) {
		lock.lock();
		logger.debug("chiamataTerminata("+id+", "+tipo.name()+") - Inizio");
		try {
			logger.debug("chiamataTerminata("+id+") entro nel try");
			liberaPostazione(id, tipo);
			chiamateTotaliServite++;
			if( tipo.name().equals ( (Tipologia.AssistenzaGuasto.name()) ) ) {
				numOperatoriAssistenza--;
			}else{
				numOperatoriInformazioni--;
			}
			logger.trace("chiamataTerminata("+id+", "+tipo.name()+") - postazione liberata, sveglio tutti i thread in attesa");
			
		} catch (Exception e) {
			logger.error("chiamataTerminata("+id+","+tipo.name()+") - Errore "+e);
		}finally{
			logger.debug("chiamataTerminata("+id+", "+tipo.name()+") - Fine");
			logger.trace("TOTALE CHIAMATE ATTUALMENTE SERVITE: "+chiamateTotaliServite);
			lock.unlock();
		}
	}
	
	private void liberaPostazione(Integer id, Tipologia tipo) {
		logger.trace("liberaPostazione("+id+") - Inizio");
		for(int i=0; i<postazioni.length; i++){
			logger.trace("liberaPostazione("+id+", "+tipo.name()+") -  la postazione "+i+" contiente "+postazioni[i].getIdChiamataServita());
			if(postazioni[i].getIdChiamataServita()==id){
				//cancella id chiamata appena servita
				postazioni[i].setIdChiamataServita(-1);
				//controllo se deve andare in pausa
				numPostazioniLibere++;
				logger.info("\t"+Operazione.FNCH.name()+"\t"+System.currentTimeMillis()+"\t"+id+"\t"+tipo+"\t"+i);
				controllaPausaPostazione(i);
				operatoriOccupati.signalAll();
				break;
			}
		}
		logger.trace("liberaPostazione("+id+") - Fine");
	}
	
	private void controllaPausaPostazione(Integer idPostazione){
		logger.trace("controllaPausaPostazione("+idPostazione+") - Inizio");
		if(postazioni[idPostazione].getChiamateRicevute() < postazioni[idPostazione].getLimiteChiamateMassime()){
			postazioni[idPostazione].setDisponibile(true);
			logger.trace("controllaPausaPostazione("+idPostazione+") - postazione ancora a lavoro");
			return;
		}
		
		logger.trace("controllaPausaPostazione("+idPostazione+") - postazione in pausa");
		//svegliamo la postazione attualmente in pausa
		if(postazioneInPausa!= -1){
			postazioni[postazioneInPausa].setDisponibile(true);
			postazioni[postazioneInPausa].azzeraChiamateRicevute();
		}else{
			//La prima volta che qualcuno entra in pausa � necessario ridurre di 1 il numero di operatori disponibili
			numPostazioniLibere--;
		}
		logger.info("\t"+Operazione.INIPAU.name()+"\t"+System.currentTimeMillis()+"\t"+idPostazione+"\t"+postazioneInPausa);
		//modifichiamo l'id della postazione attualmente in pausa
		postazioneInPausa=idPostazione;
		logger.trace("controllaPausaPostazione("+idPostazione+") - Fine");
	}

}
