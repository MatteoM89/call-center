/**
 * 
 */
package it.cybersec.af2.gruppo3.callcenter.infrastruttura;

/**
 * Support object for the management of the Call Center
 * 
 * @author Matteo Mungo
 * @author Clemente Palermo
 * @author Ramona Procopio
 *
 */
class Postazione {
	
	private Integer id;
	private boolean disponibile;
	private Integer numChiamateElaborate;
	private Integer idChiamataServita;
	
	private static int LIMITE_CHIAMATE_PAUSA;
	
	/**
	 * Constructor with parameters
	 * @param Integer id
	 * @param int limit of the calls before a coffee-break
	 */
	public Postazione(Integer id, int lcp) {
		this.id=id;
		this.disponibile=true;
		this.numChiamateElaborate=0;
		this.idChiamataServita=-1;
		
		LIMITE_CHIAMATE_PAUSA=lcp;
	}
	
	public Integer getId() {
		return id;
	}

	public boolean isDisponibile() {
		return disponibile;
	}

	public void setDisponibile(boolean disponibile) {
		this.disponibile = disponibile;
	}

	public Integer getChiamateRicevute() {
		return numChiamateElaborate;
	}

	public void aggiungiChiamataRicevuta() {
		this.numChiamateElaborate ++;
	}
	
	public void azzeraChiamateRicevute(){
		this.numChiamateElaborate=0;
	}

	public int getLimiteChiamateMassime() {
		return LIMITE_CHIAMATE_PAUSA;
	}

	public Integer getIdChiamataServita() {
		return idChiamataServita;
	}

	public void setIdChiamataServita(Integer idChiamataServita) {
		this.idChiamataServita = idChiamataServita;
	}


}
