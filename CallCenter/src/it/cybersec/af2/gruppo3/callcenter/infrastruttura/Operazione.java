package it.cybersec.af2.gruppo3.callcenter.infrastruttura;

public enum Operazione {
	ARVCD,	//Arrivo in coda
	USTCD,	//Uscita dalla coda - inizio chiamata con l'operatore
	FNCH,	//Fine della chiamata
	RFTCH,	//Rifiuto chiamata
	INIPAU,	//Inizio di una pausa (fine della precedente)
	INIT,	//Inizializzazione
}
